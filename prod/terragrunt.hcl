# Provider & Region
generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
provider "aws" {
  region = "eu-central-1"
}
EOF
}

# Include Parent DIR 
include "root "{
  path = find_in_parent_folders()
}

# Source of TF Code
terraform {
  source = "tfr:///terraform-aws-modules/ec2-instance/aws?version=3.4.0"
}

# Input Variables
inputs = {
  name = "webserver"
  ami = "ami-0eb7496c2e0403237" # Amazon Linux
  instance_type = "t2.small"
  user_data     = "${file("webserver.sh")}"
  
  tags = {
    Name      = "webserver-prod"
    Terraform = "true"
    Environment = "prod"
  }
}
